Prerequisites  
====================


.. include:: image.rst
.. include:: icon.rst



|global_screen|


Flow
-----------

* :term:`Flow` = number of movements or quantity of goods transferred between two machines (From - To). The :term:`graphical view` represents the directional flow (From - To). The :term:`real view` represents the flow on each segment without added indication of direction.

* Flow expressed in terms of **movement** (movement= transfer of a batch of parts from a position A to a position B): the value of the flux is the number of trips made to move all parts of a position A to a position B.

* Flow expressed in terms of **quantity** : The flow value is the sum of all parts moved from a position A to a position B.


Machine
-----------

* SIMOGGA considers a machine, all machines, workstations, and storage locations where the product is stopped, (it can be transformed by an operation of the process or just stored for some time).
* The machines have a **color** which characterizes the type of machine.
* The level of **filling** of the colored part is the percentage of the load with respect to the capacity.

Views
-----------

* SIMOGGA is organized into different views: :term:`graphical view`,:term:`design view`,:term:`interaction view`,:term:`scheduler view`.
* **Graphical** view : Visualization of directional flow without constraint (cultural, technical, historical).
* **Design** view: View that permit the plant layout definition.
* **Interaction** view : View with the plant layout to account for technical constraints of the plant (plant areas, entry-exit, immovable machinery).
* **Scheduler** view: View used to make diffrent scheduling manipulations.

Scenario
---------------------------------

* SIMOGGA is built based on a set of customer data = the Excel file where each transaction is assigned to a machine. This corresponds to a particular Operation-Machine solution. The assignment of operations on machines is used to define the matrix of From-To stream where all flows of a machine A to a B machines are represented.
* Each :term:`scenario` presented in SIMOGGA corresponds to a solution **Machine-Operation** particular. This solution involves the flow between different machines and a specific use machine (with respect to the load capacity defined). 
* Each :term:`scenario` is also defined by a  factory **design**.