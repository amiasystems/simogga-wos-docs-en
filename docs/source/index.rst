.. SIMOGGA WOS documentation master file, created by
   sphinx-quickstart on Tue Aug 26 13:57:17 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SIMOGGA WOS's documentation!
=========================================

TO BE ADDED

SIMOGGA key concepts

* TO ADD
* TO ADD



.. note:: Don't forget that you can contact us at anytime via this page: http://www.amia-systems.com/contact


Contents:

.. toctree::
   :maxdepth: 2
   
   prerequisite
   glossary
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex`